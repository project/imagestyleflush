
Image Style Flush
==================

This module allows you to flush all image styles at once from the administration (similar like `drush if` works).

In Drupal 7 version was implemented the solution to be able to flush styles individually,
but this in Drupal 8~10 is core, so it is not necessary.

Features
--------

  - Flush all image styles


Dependencies
------------

The image (core) module.


Install
-------

1) Perform a composer require of the module as normally done.

2) On your Drupal site, activate the module under Administration -> Modules
   (/admin/modules).

3) Go to the Administration -> People -> Permissions page to give the access to the image styles.

Usage
-----

You can clear the image styles under Administration -> Settings -> Media -> Image Styles-> Image Styles

Note that this module only flushes images. It does not rebuild them.


Credits
------

This module was written by Stepan Kuzmin and is maintained by Hargobind Khalsa and dcimorra.