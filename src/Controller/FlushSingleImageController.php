<?php

namespace Drupal\imagestyleflush\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImageStyleFlushController.
 *
 * Controller to flush image styles in batches.
 */
class FlushSingleImageController extends ControllerBase {

  use StringTranslationTrait;

  const IMAGE_STYLE_COLLECTION = 'entity.image_style.collection';

  const LOGGER_CHANNEL = 'flush_single_image';

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ImageStyleFlushController object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, TranslationInterface $string_translation, EntityTypeManagerInterface $entity_type_manager) {
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('string_translation'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Starts the batch process to flush image styles.
   */
  public function flushImageStyles() {
    // Load all image styles from the system.
    $image_styles = $this->entityTypeManager->getStorage('image_style')->loadMultiple();

    // Create a new batch builder.
    $batch_builder = new BatchBuilder();

    // Set batch title and messages.
    $batch_builder->setTitle($this->t('Flushing image styles'))
      ->setInitMessage($this->t('Starting the flushing process...'))
      ->setProgressMessage($this->t('Flushing image styles: @current out of @total.'))
      ->setErrorMessage($this->t('An error occurred during the flushing process.'));

    // Add a batch operation for each image style.
    foreach ($image_styles as $style_name => $image_style) {
      $batch_builder->addOperation([__CLASS__, 'flushImageStyle'], [$style_name]);
    }

    // Set a callback to handle completion.
    $batch_builder->setFinishCallback([__CLASS__, 'finishBatch']);

    // Set the batch.
    batch_set($batch_builder->toArray());

    // Return a redirect to the batch processing page.
    return batch_process(Url::fromRoute(self::IMAGE_STYLE_COLLECTION));
  }

  /**
   * Flushes a single image style.
   *
   * @param string $style_name
   *   The name of the image style to be flushed.
   * @param array $context
   *   The batch context.
   */
  public static function flushImageStyle($style_name, &$context) {
    $image_style = \Drupal::entityTypeManager()->getStorage('image_style')->load($style_name);
    if ($image_style) {
      $image_style->flush();
      \Drupal::logger(self::LOGGER_CHANNEL)->info('Flushed image style: @style', ['@style' => $style_name]);

      $context['message'] = t('Flushing image style: @style', ['@style' => $style_name]);
    }
  }

  /**
   * Callback to handle batch completion.
   *
   * @param bool $success
   *   Whether the batch process was successful.
   * @param array $results
   *   The batch results.
   * @param array $operations
   *   The remaining batch operations.
   */
  public static function finishBatch($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('All image styles have been successfully flushed.'));
    }
    else {
      $messenger->addMessage(t('There was an issue flushing some image styles.'));
    }
  }

}
